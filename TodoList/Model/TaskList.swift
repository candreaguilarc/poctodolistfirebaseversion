//
//  TaskList.swift
//  TodoList
//
//  Created by Carlo Andre Aguilar Castrat on 1/17/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit
import Firebase

class TaskList: NSObject {
//    var name = ""
//    var tasks: [Task] = []
//    var iconName = ""
//
//    init(name: String) {
//        super.init()
//        self.name = name
//    }
    
    var ref: DatabaseReference?
    var key: String
    var name = ""
    var tasks: [Task] = []
    var iconName = ""

    init(name: String, key: String = "", iconName: String) {
        self.ref = nil
        self.key = key
        self.name = name
        self.iconName = iconName
    }

    init?(snapshot: DataSnapshot) {
        let tasksDict = (snapshot.value as! [String: AnyObject])["tasks"] as? [String: AnyObject]
        guard
            let value = snapshot.value as? [String: AnyObject],
            let name = value["name"] as? String,
            let iconName = value["iconName"] as? String
            else {
                return nil
        }
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.name = name
        self.iconName = iconName
        
        let tasksRef = self.ref?.child("tasks")
        
        for task in tasksDict ?? [:] {
            let newTask = Task(name: task.value["name"] as? String ?? "", hasBeenCompleted: task.value["hasBeenCompleted"] as? Bool ?? false, key: task.key)
            let taskRef = tasksRef?.child(task.key)
            newTask.ref = taskRef
            self.tasks.append(newTask)
        }
    }

    func toAnyObject() -> Any {
        return [
            "name": name,
            "tasks": tasks,
            "iconName": iconName
        ]
    }
}
