//
//  Task.swift
//  TodoList
//
//  Created by Carlo Andre Aguilar Castrat on 1/17/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//


import UIKit
import Firebase

class Task: NSObject {
    var ref: DatabaseReference?
    var key: String
    let name: String
    var hasBeenCompleted = false

    init(name: String, hasBeenCompleted: Bool, key: String = "") {
        self.ref = nil
        self.key = key
        self.name = name
        self.hasBeenCompleted = hasBeenCompleted
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let name = value["name"] as? String,
            let hasBeenCompleted = value["hasBeenCompleted"] as? Bool else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.name = name
        self.hasBeenCompleted = hasBeenCompleted
    }
    
    func toAnyObject() -> Any {
        return [
            "name": name,
            "hasBeenCompleted": hasBeenCompleted
        ]
    }
}
