//
//  FirebaseData.swift
//  POCTodoList
//
//  Created by Carlo Aguilar on 2/21/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import Foundation
import Firebase

class FirebaseData {
    static let shared = FirebaseData()
    
    var userRef: DatabaseReference?
    var allTaskLists: [TaskList] = []
    var userEmail: String?
    
    // MARK: - Private
    
    func setUserReference(){
        self.userRef = Database.database().reference(withPath: "Users/" + Auth.auth().currentUser!.uid)
        self.userEmail =  Auth.auth().currentUser?.email ?? ""
    }
    
    func updateData(finished: @escaping () -> ()){
        userRef?.observe(.value, with: { snapshot in
            var newTaskLists: [TaskList] = []
            for child in snapshot.children {
                let snapshot = child as? DataSnapshot
                let taskList = TaskList(snapshot: snapshot ?? DataSnapshot())
                let sortedTasks = taskList?.tasks.sorted(by: { $0.key < $1.key })
                taskList?.tasks = sortedTasks ?? []
                newTaskLists.append(taskList ?? TaskList(name: "", iconName: ""))
            }
            self.allTaskLists = newTaskLists
            finished()
        })
    }

    
    func createDefaultTaskLists(){
        let todayTaskList = TaskList(name: "Today", iconName: "today")
        let todayTaskListRef = self.userRef?.childByAutoId()
        todayTaskList.ref = todayTaskListRef
        todayTaskList.key = todayTaskListRef?.key ?? ""
        todayTaskListRef?.setValue(todayTaskList.toAnyObject())
        
        let personalTaskList = TaskList(name: "Personal", iconName: "personal")
        let personalTaskListRef = self.userRef?.childByAutoId()
        personalTaskList.ref = personalTaskListRef
        personalTaskList.key = personalTaskListRef?.key ?? ""
        personalTaskListRef?.setValue(personalTaskList.toAnyObject())
        
        let workTaskList = TaskList(name: "Work", iconName: "work")
        let workTaskListRef = self.userRef?.childByAutoId()
        workTaskList.ref = workTaskListRef
        workTaskList.key = workTaskListRef?.key ?? ""
        workTaskListRef?.setValue(workTaskList.toAnyObject())
        
        let todayTasksRef = Database.database().reference(withPath: "Users/" + Auth.auth().currentUser!.uid + "/" + todayTaskList.key + "/tasks")
        let task1Ref = todayTasksRef.childByAutoId()
        let task1 = Task(name: "Install the TodoList app", hasBeenCompleted: true)
        task1.key = task1Ref.key ?? ""
        task1.ref = task1Ref
        task1Ref.setValue(task1.toAnyObject())
        
        let task2Ref = todayTasksRef.childByAutoId()
        let task2 = Task(name: "Finish homework", hasBeenCompleted: false)
        task2.key = task2Ref.key ?? ""
        task2.ref = task1Ref
        task2Ref.setValue(task2.toAnyObject())
        
        todayTaskList.tasks.append(task1)
        todayTaskList.tasks.append(task2)
        self.allTaskLists = [todayTaskList, personalTaskList, workTaskList]
    }
    
}
