//
//  CardCellCollectionViewCell.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 1/24/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

class CardCell: UICollectionViewCell {
    internal static let cellHeight: CGFloat = 2.0
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var taskCountLabel: UILabel!
    @IBOutlet weak var listNameLabel: UILabel!
    @IBOutlet weak var taskListProgressView: UIProgressView!
    @IBOutlet weak var taskListProgressLabel: UILabel!
    
    var cardViewSafeArea: UIEdgeInsets = .zero
    
    //Mark: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
   }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 10
        self.layer.shadowRadius = 7
        self.layer.shadowOffset = CGSize(width: 0, height: 15)
        self.layer.shadowOpacity = 0.6
    }
    
}
