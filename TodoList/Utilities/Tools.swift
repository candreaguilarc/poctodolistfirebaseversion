//
//  Tools.swift
//  POCTodoList
//
//  Created by Carlo Aguilar on 2/1/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import Foundation
import UIKit

class Tools {
    
    func setTasksProgress(tasks: [Task], progressView: UIProgressView, progressLabel: UILabel){
        UIView.animate(withDuration: 0.4) {
            progressView.setProgress(self.getTasksProgress(tasks: tasks), animated: true)
        }
        progressLabel.text = String(format: "%.f", (progressView.progress * 100)) + "%"
    }
    
    func getTasksProgress(tasks: [Task]) -> Float{
        if tasks.count == 0 {
            return 0
        }
        var completedTasksCounter: Float = 0.0
        for task in tasks {
            if task.hasBeenCompleted == true{
                completedTasksCounter += 1.0
            }
        }
        return Float(completedTasksCounter/Float(tasks.count))
    }
    
    func setTaskCountLabel(tasks: [Task], label: UILabel){
        var notCompletedTasksCounter = 0
        for task in tasks{
            if task.hasBeenCompleted == false{
                notCompletedTasksCounter += 1
            }
        }
        if notCompletedTasksCounter == 1 {
            label.text = "1 Task left"
        } else if notCompletedTasksCounter == 0 {
            label.text = "No Tasks left"
        }else {
            label.text = String(notCompletedTasksCounter) + " Tasks left"
        }
    }
    
}
