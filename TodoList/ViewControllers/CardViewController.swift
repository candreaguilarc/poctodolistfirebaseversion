//
//  cardViewController.swift
//  POCTodoList
//
//  Created by Carlo Aguilar on 1/29/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit
import Firebase

protocol DataDelegate: class {
    func setTotalTasksLabel()
    func setDateLabel(date : Date)
}

class CardViewController: UIViewController, NewTaskDelegate {
    let presentNewTaskAnimationController = PresentNewTaskViewAnimationController()
    let dismissNewTaskAnimationController = DismissNewTaskViewAnimationController()
    let segueIdentifier = "PresentNewTaskView"
    let tableViewCellIdentifier = "TaskCell"
    
    @IBOutlet weak var newTaskButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tasksTableView: UITableView!
    @IBOutlet weak var cardViewContainer: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var infoStackView: UIStackView!
    @IBOutlet weak var cardNameLabel: UILabel!
    @IBOutlet weak var taskCountLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var taskListProgressView: UIProgressView!
    @IBOutlet weak var taskListProgressLabel: UILabel!
    
    @IBOutlet weak var iconImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var iconImageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewContainerLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewContainerTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoStackViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoStackViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoStackViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoStackViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var tasksTitleLabel: UILabel!
    
    
    var selectedTaskListIndex = 0
    var tools = Tools()
    weak var delegate: DataDelegate?
    var tasksRef: DatabaseReference?
    
    var initialAnimationPositionCardViewConstraints: [NSLayoutConstraint] = []
    var finalAnimationPositionCardViewConstraints: [NSLayoutConstraint] = []
    var initialAnimationPositionCardViewContainerConstraints: [NSLayoutConstraint] = []
    var finalAnimationPositionCardViewContainerConstraints: [NSLayoutConstraint] = []

    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.newTaskButton.layer.cornerRadius = self.newTaskButton.frame.size.height / 2
        self.tools.setTasksProgress(tasks: FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks, progressView: self.taskListProgressView, progressLabel: self.taskListProgressLabel)
        self.cardNameLabel.text = FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].name
        self.iconImageView.image = UIImage(named: FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].iconName)
        self.tools.setTaskCountLabel(tasks: FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks, label: taskCountLabel)
        tasksRef = Database.database().reference(withPath: "Users/" + Auth.auth().currentUser!.uid + "/" + FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].key + "/tasks")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? NewTaskViewController{
            destinationViewController.transitioningDelegate = self
            destinationViewController.delegate = self
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func backButtonTouchUpInside(_ sender: UIButton) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
        self.delegate?.setTotalTasksLabel()
        self.delegate?.setDateLabel(date: Date())
    }
    
    @IBAction func newTaskButtonTouchUpInside(_ sender: Any) {
        self.performSegue(withIdentifier: self.segueIdentifier, sender: self)
    }
    
    //MARK: - Private
    
    func toggleCellCheckbox(_ cell: TaskCell, isCompleted: Bool) {
        if isCompleted {
            cell.accessoryType = .none
            cell.nameLabel?.textColor = .black
        } else {
            cell.accessoryType = .checkmark
            cell.nameLabel?.textColor = .gray
        }
    }
    
    //MARK: - Public
    
    func addTask(task: Task){
        let taskRef = self.tasksRef?.childByAutoId()
        task.key = taskRef?.key ?? ""
        task.ref = taskRef
        taskRef?.setValue(task.toAnyObject())
        
        FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks.append(task)
        self.tasksTableView.reloadData()
        self.tools.setTaskCountLabel(tasks: FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks, label: self.taskCountLabel)
        self.tools.setTasksProgress(tasks: FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks, progressView: self.taskListProgressView, progressLabel: self.taskListProgressLabel)
    }
}

extension CardViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.tableViewCellIdentifier, for: indexPath) as? TaskCell else {
            return UITableViewCell()
        }
        cell.nameLabel.text = FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks[indexPath.row].name
        let task = FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks[indexPath.row]
        if(task.hasBeenCompleted == false) {
            self.toggleCellCheckbox(cell, isCompleted: true)
        }else{
            self.toggleCellCheckbox(cell, isCompleted: false)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks.count == 0 {
            self.tasksTableView.setEmptyMessage("You have no tasks yet")
        } else {
            self.tasksTableView.restore()
        }
        return FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tasksTableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath) as? TaskCell {
            let task = FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks[indexPath.row]
            if(task.hasBeenCompleted == false) {
                self.toggleCellCheckbox(cell, isCompleted: false)
                FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks[indexPath.row].hasBeenCompleted = true
            }else{
                self.toggleCellCheckbox(cell, isCompleted: true)
                FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks[indexPath.row].hasBeenCompleted = false
            }
            task.ref?.updateChildValues([
                "hasBeenCompleted": (task.hasBeenCompleted)
                ])
        }
        
        self.tools.setTasksProgress(tasks: FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks, progressView: self.taskListProgressView, progressLabel: self.taskListProgressLabel)
        self.tools.setTaskCountLabel(tasks: FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks, label: self.taskCountLabel)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?{
        let task = FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks[indexPath.row]
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks.remove(at: indexPath.row)
            task.ref?.removeValue()
            self.tasksTableView.deleteRows(at: [indexPath], with: .fade)
            self.tools.setTaskCountLabel(tasks: FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks, label: self.taskCountLabel)
            self.tools.setTasksProgress(tasks: FirebaseData.shared.allTaskLists[self.selectedTaskListIndex].tasks, progressView: self.taskListProgressView, progressLabel: self.taskListProgressLabel)
        }
        deleteAction.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        return [deleteAction]
    }
}

extension CardViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.presentNewTaskAnimationController
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.dismissNewTaskAnimationController
    }
}

extension CardViewController {
    
    func cardViewPositionConstraints(left: CGFloat, right: CGFloat, top: CGFloat, bottom: CGFloat) {
        self.view.layoutIfNeeded()
        
        self.cardViewLeadingConstraint.constant = left
        self.cardViewTrailingConstraint.constant = right
        self.cardViewTopConstraint.constant = top
        self.cardViewBottomConstraint.constant = bottom
        self.view.layoutIfNeeded()
    }
    
    func cardViewContainerPositionConstraints(left: CGFloat, right: CGFloat, top: CGFloat, bottom: CGFloat) {
        self.view.layoutIfNeeded()
        self.cardViewContainerLeadingConstraint.constant = left
        self.cardViewContainerTrailingConstraint.constant = right
        self.cardViewContainerTopConstraint.constant = top
        self.cardViewContainerBottomConstraint.constant = bottom
        self.view.layoutIfNeeded()
        
    }
    
    func infoStackViewMoveDown(){
        self.infoStackViewTopConstraint.isActive = false
        self.infoStackViewBottomConstraint.isActive = true
        self.infoStackViewBottomConstraint.constant = 20
        self.infoStackViewLeadingConstraint.constant = 20
        self.infoStackViewTrailingConstraint.constant = 20
        self.view.layoutIfNeeded()
    }
    
    func infoStackViewMoveUp(){
        self.infoStackViewBottomConstraint.isActive = false
        self.infoStackViewTopConstraint.isActive = true
        self.infoStackViewTopConstraint.constant = 10
        self.infoStackViewLeadingConstraint.constant = 40
        self.infoStackViewTrailingConstraint.constant = 40
        self.view.layoutIfNeeded()
    }
    
    func iconImageViewMoveDown(){
        self.iconImageViewHeightConstraint =
            self.iconImageViewHeightConstraint.setMultiplier(multiplier: 1.8)
        self.iconImageViewTopConstraint.constant = 70
        self.view.layoutIfNeeded()
    }
    
    func iconImageViewMoveUp(){
        self.iconImageViewHeightConstraint =
            self.iconImageViewHeightConstraint.setMultiplier(multiplier: 2.3)
        self.iconImageViewTopConstraint.constant = 20
        self.view.layoutIfNeeded()
    }
    
    func backButtonNotVisible() {
        self.backButton.alpha = 0
    }
    
    func newTaskButtonNotVisible(){
        self.newTaskButton.alpha = 0
    }
    
    func newTaskButtonVisible(){
        self.newTaskButton.alpha = 1.0
    }
    
    func backButtonVisible() {
        self.backButton.alpha = 1.0
    }
}
