//
//  SplashScreenViewController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/15/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit
import Firebase

class SplashScreenViewController: UIViewController {
    let splashScreenToLoginScreenAnimationController = SplashScreenToLoginScreenAnimationController()
    let splashScreenToHomeScreenAnimationController = SplashScreenToHomeScreenAnimationController()
    
    let loginSegueIdentifier = "SplashScreenToLogin"
    let homeSegueIdentifier = "SplashScreenToHome"
    
    @IBOutlet weak var notepadImageView: UIImageView!
    
    //MARK: - Lifecycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Auth.auth().currentUser != nil {
            FirebaseData.shared.setUserReference()
            FirebaseData.shared.updateData {
                DispatchQueue.main.async {
                    if FirebaseData.shared.allTaskLists == [] {
                        FirebaseData.shared.createDefaultTaskLists()
                    } else {
                        self.performSegue(withIdentifier: self.homeSegueIdentifier, sender: nil)
                    }
                }
            }
            
        } else{
            self.performSegue(withIdentifier: self.loginSegueIdentifier, sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? LoginViewController {
            destinationViewController.transitioningDelegate = self
        } else if let destinationViewController = segue.destination as? POCBaseViewController {
            destinationViewController.transitioningDelegate = self
        }
    }
    
}

extension SplashScreenViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if let _ = presented as? LoginViewController {
            return self.splashScreenToLoginScreenAnimationController
        } else {
            return self.splashScreenToHomeScreenAnimationController
        }
    }
}
