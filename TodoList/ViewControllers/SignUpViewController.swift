//
//  SignUpViewController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/15/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController, UITextFieldDelegate {
    let signUpScreenToHomeScreenAnimationController = SignUpScreenToHomeScreenAnimationController()
    let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var notepadImageView: UIImageView!
    @IBOutlet weak var gradientView: GradientView!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var textStackView: UIStackView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtRetypePassword: UITextField!
    
    var keyboardIsVisible = false
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
        activityIndicator.center = self.view.center
        self.view.addSubview(activityIndicator)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? POCBaseViewController {
            destinationViewController.transitioningDelegate = self
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func loginButtonTouchUpInside(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.keyboardIsVisible == true {
            if (self.keyboardDoneHiding() == true) {
                self.presentingViewController?.dismiss(animated: true, completion: nil)
            }
        } else {
           self.presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func signInAction(_ sender: Any) {
        self.signUp()
    }
    
    //MARK: - Private
    
    func signUp () {
        if txtPassword.text != txtRetypePassword.text {
            let alertController = UIAlertController(title: "Password Incorrect", message: "Please re-type password", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            self.activityIndicator.startAnimating()
            Auth.auth().createUser(withEmail: txtEmail.text!, password: txtPassword.text!){ (user, error) in
                if error == nil {
                    FirebaseData.shared.setUserReference()
                    FirebaseData.shared.updateData {
                        DispatchQueue.main.async {
                            if FirebaseData.shared.allTaskLists == [] {
                                FirebaseData.shared.createDefaultTaskLists()
                                self.performSegue(withIdentifier: "SignUpToHome", sender: nil)
                                
                            } else {
                                self.performSegue(withIdentifier: "SignUpToHome", sender: nil)
                            }
                            self.activityIndicator.stopAnimating()
                        }
                    }
                }
                else{
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                    self.activityIndicator.stopAnimating()
                }
            }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.keyboardIsVisible = true
    }
    
    @objc func keyboardWillChange(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y = 0 - keyboardSize.height
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func keyboardDidHide(notification: NSNotification) {
        self.keyboardIsVisible = false
    }
    
    func keyboardDoneHiding() -> Bool {
        var finish = false
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardDidHideNotification, object: nil, queue: nil, using: { note in
            finish = true
        })
        while !finish {
            RunLoop.current.run(until: Date(timeIntervalSinceNow: 0.01))
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = self.view.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            self.signUp()
        }
        return false
    }
    
    deinit {
        
    }
}

extension SignUpViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.signUpScreenToHomeScreenAnimationController
    }
}
