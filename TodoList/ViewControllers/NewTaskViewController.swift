//
//  NewTaskViewController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/6/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

protocol NewTaskDelegate: class {
    func addTask(task: Task)
}

class NewTaskViewController: UIViewController {
    @IBOutlet weak var taskNameTextField: UITextField!
    @IBOutlet weak var newTaskButton: UIButton!
    @IBOutlet weak var newTaskContainerView: UIView!
    @IBOutlet weak var newTaskButtonTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var newTaskButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var newTaskButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var newTaskContainerViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var newTaskContainerViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var newTaskContainerViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var newTaskContainerViewTrailingConstraint: NSLayoutConstraint!
    
    weak var delegate: NewTaskDelegate?
    var newTaskButtonLeadingConstraint2: NSLayoutConstraint?
    var keyboardHeight: CGRect = .zero
    var keyboardAnimationDuration = 0.0
    var keyboardAnimationCurve = 0
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK: - IBActions
    
    @IBAction func addNewTaskTouchUpInside(_ sender: Any) {
        self.addTask()
    }
    
    @IBAction func newTaskTextFieldPrimaryActionTriggered(_ sender: UITextField) {
        self.addTask()
    }
    
    @IBAction func dismissButton(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Private
    
    @objc func keyboardWillChange(notification: NSNotification) {
        let bottomDistanceFromRootView = self.view.frame.height - self.newTaskContainerView.frame.origin.y - self.newTaskContainerView.frame.height
        let info = notification.userInfo!
        let duration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double) ?? 0.0
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        if keyboardFrame.height > bottomDistanceFromRootView {
            UIView.animate(withDuration: duration, animations: {
                self.newTaskButtonBottomConstraint.constant = keyboardFrame.size.height - bottomDistanceFromRootView
            })
        } else {
            UIView.animate(withDuration: duration, animations: {
                self.newTaskButtonBottomConstraint.constant = 0
                
            })
        }
        self.view.layoutIfNeeded()
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let duration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double) ?? 0.0
        UIView.animate(withDuration: duration, animations: {
            self.newTaskButtonBottomConstraint.constant = 0
            
        })
        self.view.layoutIfNeeded()
    }
    
    func addTask(){
        if taskNameTextField.text?.trimmingCharacters(in: .whitespaces).count != 0 {
            let task = Task(name: self.taskNameTextField.text ?? "", hasBeenCompleted: false)
            delegate?.addTask(task: task)
            self.view.endEditing(true)
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
  
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension NewTaskViewController {

    func newTaskContainerViewConstraints(left: CGFloat, right: CGFloat, top: CGFloat, bottom: CGFloat) {
        self.view.layoutIfNeeded()
        self.newTaskContainerViewLeadingConstraint.constant = left
        self.newTaskContainerViewTrailingConstraint.constant = right
        self.newTaskContainerViewTopConstraint.constant = top
        self.newTaskContainerViewBottomConstraint.constant = bottom
        self.view.layoutIfNeeded()
    }
    
    func setNewTaskButtonInitialPosition(){
        self.newTaskButton.layer.cornerRadius = self.newTaskButton.frame.size.height / 2
        self.newTaskButtonBottomConstraint.constant = 20
        self.newTaskButtonWidthConstraint.constant = 60
        if self.newTaskButtonTrailingConstraint == nil {
            self.newTaskButton.rightAnchor.constraint(equalTo: self.newTaskContainerView.rightAnchor, constant: -40).isActive = true
        } else {
            self.newTaskButtonTrailingConstraint.constant = 40
        }
        self.view.layoutIfNeeded()
    }
    
    func expandNewTaskButton(){
        if self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular && self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.regular {
            self.newTaskButtonWidthConstraint.constant = self.newTaskContainerView.frame.width
        } else {
            self.newTaskButtonWidthConstraint.constant = self.newTaskContainerView.frame.width
        }
        self.newTaskButtonBottomConstraint.constant = 0
        self.newTaskButtonTrailingConstraint.constant = 0
        let makeCornersSquareAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.cornerRadius))
        makeCornersSquareAnimation.fromValue = self.newTaskButton.frame.size.height / 2
        makeCornersSquareAnimation.toValue = 0
        makeCornersSquareAnimation.duration = 0.5
        self.newTaskButton.layer.add(makeCornersSquareAnimation, forKey: "cornerRadius")
        self.newTaskButton.layer.cornerRadius = 0
        self.view.layoutIfNeeded()
    }
    
}
