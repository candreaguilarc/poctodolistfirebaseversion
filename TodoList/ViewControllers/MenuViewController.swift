//
//  MenuViewController.swift
//  POCTodoList
//
//  Created by Carlo Aguilar on 2/23/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase

class MenuViewController: UIViewController {
    let initialViewControllerIdentifier = "StartViewController"
    var delegate: POCBaseViewController!
    
    @IBOutlet weak var optionsTableView: UITableView!
    
    //MARK: - Private
   
    override var preferredContentSize : CGSize {
        get {
            return CGSize(width: 230 , height: 75)
        }
        set {
            super.preferredContentSize = newValue
        }
    }
    
    func logOut(){
        do {
            GIDSignIn.sharedInstance()?.signOut()
            try Auth.auth().signOut()
        }
        catch {
            return
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initial = storyboard.instantiateViewController(withIdentifier: self.initialViewControllerIdentifier)
        UIApplication.shared.keyWindow?.rootViewController = initial
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.alpha = 0
        self.popoverPresentationController?.containerView?.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.25) {
           self.view.alpha = 1
            self.popoverPresentationController?.containerView?.alpha = 1
        }
    }
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmailCell", for: indexPath) as? EmailCell else {
                return UITableViewCell()
            }
            cell.emailLabel.text = FirebaseData.shared.userEmail
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "LogOutCell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.optionsTableView.deselectRow(at: indexPath, animated: true)
        } else {
            FirebaseData.shared.userRef?.removeAllObservers()
            self.logOut()
        }
    }
}
