//
//  StartViewController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/14/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn


class LoginViewController: UIViewController, UITextFieldDelegate, GIDSignInDelegate, GIDSignInUIDelegate {
    let presentSignUpAnimationController = PresentSignUpAnimationController()
    let dismissSignUpAnimationController = DismissSignUpAnimationController()
    let loginScreenToHomeScreenAnimationController = LoginScreenToHomeScreenAnimationController()
    let signUpSegueIdentifier = "LoginToSignUp"
    let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    
    @IBOutlet weak var middleStackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var gradientView: GradientView!
    @IBOutlet weak var notepadImageView: UIImageView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var notepadImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var notepadImageViewYConstraint: NSLayoutConstraint!
    
    var keyboardIsVisible = false
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
        activityIndicator.center = self.view.center
        self.view.addSubview(activityIndicator)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? SignUpViewController {
            destinationViewController.transitioningDelegate = self
        } else if let destinationViewController = segue.destination as? POCBaseViewController{
            destinationViewController.transitioningDelegate = self
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func signUpButtonTouchUpInside(_ sender: UIButton) {
        self.view.endEditing(true)
        self.view.frame.origin.y = 0
        if self.keyboardIsVisible == true {
            if (self.keyboardDoneHiding() == true) {
                self.performSegue(withIdentifier: self.signUpSegueIdentifier, sender: self)
            }
        } else {
            self.performSegue(withIdentifier: self.signUpSegueIdentifier, sender: self)
        }
    }
    
    @IBAction func logInAction(_ sender: Any) {
        self.logInRegularly()
    }
    
    @IBAction func googleSignIn(_ sender: AnyObject) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    //MARK: - Private
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            return
        }
        guard let authentication = user.authentication else { return }
         self.activityIndicator.startAnimating()
        let credential = GoogleAuthProvider.credential(withIDToken: (authentication.idToken)!, accessToken: (authentication.accessToken)!)
        Auth.auth().signInAndRetrieveData(with: credential, completion: { (user, error) in
            if error != nil {
                return
            }
            FirebaseData.shared.setUserReference()
            FirebaseData.shared.updateData {
                DispatchQueue.main.async {
                    if FirebaseData.shared.allTaskLists == [] {
                        FirebaseData.shared.createDefaultTaskLists()
                        self.performSegue(withIdentifier: "alreadyLoggedIn", sender: nil)
                    } else {
                        self.performSegue(withIdentifier: "alreadyLoggedIn", sender: nil)
                    }
                    self.activityIndicator.stopAnimating()
                }
            }
        })
        
    }
    
    func sign(_ signIn: GIDSignIn?, present viewController: UIViewController?) {
        if let aController = viewController {
            present(aController, animated: true) {() -> Void in }
        }
    }
    
    func sign(_ signIn: GIDSignIn?, dismiss viewController: UIViewController?) {
        dismiss(animated: true) {() -> Void in
        }
    }
    
    func logInRegularly(){
        self.activityIndicator.startAnimating()
        Auth.auth().signIn(withEmail: self.txtEmail.text!, password: self.txtPassword.text!) { (user, error) in
            if error == nil{
                
                
                    //do something expensive
                FirebaseData.shared.setUserReference()
                FirebaseData.shared.updateData {
                    DispatchQueue.main.async {
                        if FirebaseData.shared.allTaskLists == [] {
                            FirebaseData.shared.createDefaultTaskLists()
                            self.performSegue(withIdentifier: "alreadyLoggedIn", sender: nil)
                        } else {
                            self.performSegue(withIdentifier: "alreadyLoggedIn", sender: nil)
                        }
                        self.activityIndicator.stopAnimating()
                    }
                }
            }
            else{
                self.activityIndicator.stopAnimating()
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.keyboardIsVisible = true
    }
    
    @objc func keyboardWillChange(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y = 0 - keyboardSize.height
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func keyboardDidHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
        self.keyboardIsVisible = false
    }
    
    func keyboardDoneHiding() -> Bool {
        var finish = false
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardDidHideNotification, object: nil, queue: nil, using: { note in
            finish = true
        })
        while !finish {
            RunLoop.current.run(until: Date(timeIntervalSinceNow: 0.01))
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = self.view.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            self.logInRegularly()
        }
        return false
    }
  
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension LoginViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if let _ = presented as? SignUpViewController{
            return self.presentSignUpAnimationController
        }else {
            return self.loginScreenToHomeScreenAnimationController
        }
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.dismissSignUpAnimationController
    }
}

extension LoginViewController {
    
    internal func notepadAnimationInitialPosition(){
        self.notepadImageViewHeightConstraint = self.notepadImageViewHeightConstraint.setMultiplier(multiplier: 0.32)
        self.notepadImageViewYConstraint = self.notepadImageViewYConstraint.setMultiplier(multiplier: 1)
        self.view.layoutIfNeeded()
    }
    
    internal func notepadAnimationFinalPosition(){
        self.notepadImageViewHeightConstraint = self.notepadImageViewHeightConstraint.setMultiplier(multiplier: 0.25)
        self.notepadImageViewYConstraint = self.notepadImageViewYConstraint.setMultiplier(multiplier: 0.5)
        self.view.layoutIfNeeded()
    }
    
}
