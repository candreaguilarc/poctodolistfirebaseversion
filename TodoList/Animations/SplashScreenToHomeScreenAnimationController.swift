//
//  PresentHomeScreenFromSpashScreenAnimationController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/18/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

internal class SplashScreenToHomeScreenAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let toViewController = transitionContext.viewController(forKey: .to) as? POCBaseViewController,
        let fromViewController = transitionContext.viewController(forKey: .from) as? SplashScreenViewController else {
                return
        }
        toViewController.setAnimationConstraints()
        containerView.addSubview(toViewController.view)
        toViewController.view.bringSubviewToFront(toViewController.notepadImageView)
        toViewController.containerView.alpha = 0
        toViewController.gradientView.alpha = 0
        toViewController.notepadImageViewInitialAnimationPositionFromSplashScreen()
        fromViewController.notepadImageView.alpha = 0
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            NSLayoutConstraint.deactivate(toViewController.initialPositionFromSplashScreenNotepadImageViewConstraints)
            toViewController.notepadImageViewFinalAnimationPosition()
            toViewController.containerView.alpha = 1.0
            toViewController.gradientView.alpha = 1.0
        }) { (_) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.7
    }
    
}
