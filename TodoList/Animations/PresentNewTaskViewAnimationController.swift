//
//  PresentNewTaskViewAnimationController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/7/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

internal class PresentNewTaskViewAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        // 1
        let containerView = transitionContext.containerView
        guard let fromViewController = transitionContext.viewController(forKey: .from) as? CardViewController,
            let toViewController = transitionContext.viewController(forKey: .to) as? NewTaskViewController else {
                return
        }
        fromViewController.newTaskButtonNotVisible()
        containerView.addSubview(toViewController.view)
        toViewController.newTaskContainerView.alpha = 0
        toViewController.view.backgroundColor = .clear
        toViewController.setNewTaskButtonInitialPosition()
        let duration = transitionDuration(using: transitionContext)
        
        if fromViewController.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular && fromViewController.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.regular {
            toViewController.newTaskContainerViewConstraints(left: (fromViewController.view.frame.width * 0.35)/2,
                                                             right: (fromViewController.view.frame.width * 0.35)/2,
                                                             top: (fromViewController.view.frame.height * 0.25)/2 - toViewController.view.safeAreaInsets.top,
                                                             bottom: (fromViewController.view.frame.height * 0.25)/2)
        }
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
            toViewController.newTaskContainerView.alpha = 1.0
            toViewController.expandNewTaskButton()
            toViewController.taskNameTextField.becomeFirstResponder()
        }) { (_) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
}
