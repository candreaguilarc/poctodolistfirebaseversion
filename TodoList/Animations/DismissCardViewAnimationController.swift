//
//  DismissCardViewAnimationController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/7/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import Foundation

import UIKit

internal class DismissCardViewAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    var selectedCardFrame: CGRect = .zero
    var safeAreaWidth: CGFloat = 0.0
    var safeAreaHeight: CGFloat = 0.0
    var safeAreaTopInset: CGFloat = 0.0
    var safeAreaBottomInset: CGFloat = 0.0
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromViewController = transitionContext.viewController(forKey: .from) as? CardViewController,
            let toViewController = transitionContext.viewController(forKey: .to) as? POCBaseViewController else {
                return
        }
        let duration = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.85, initialSpringVelocity: 0, options: .curveLinear, animations: {
            toViewController.view.alpha = 1
            if toViewController.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular && toViewController.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.regular {
                toViewController.undimNeighborCells()
            }
            fromViewController.cardViewContainerPositionConstraints(left: self.selectedCardFrame.origin.x,
                                                                    right: self.safeAreaWidth - self.selectedCardFrame.origin.x - self.selectedCardFrame.width,
                                                                    top: self.selectedCardFrame.origin.y + self.safeAreaTopInset,
                                                                  bottom: toViewController.view.frame.height - toViewController.view.safeAreaInsets.top -
                                                                    self.selectedCardFrame.origin.y - self.selectedCardFrame.height)
            
            fromViewController.cardViewPositionConstraints(left: self.selectedCardFrame.origin.x,
                                                           right: self.safeAreaWidth - self.selectedCardFrame.origin.x - self.selectedCardFrame.width,
                                                         top: self.selectedCardFrame.origin.y,
                                                         bottom: self.safeAreaHeight -
                                                            self.selectedCardFrame.origin.y - self.selectedCardFrame.height)
            
            fromViewController.infoStackViewMoveDown()
            fromViewController.backButtonNotVisible()
            fromViewController.newTaskButtonNotVisible()
            fromViewController.iconImageViewMoveUp()
            fromViewController.tasksTitleLabel.alpha = 0
            fromViewController.view.backgroundColor = .clear
        }) { (_) in
            if toViewController.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular && toViewController.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.regular {
                toViewController.unhideMainCell()
            }
            toViewController.updateCollectionViewCell()
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            fromViewController.view.removeFromSuperview()
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
   
}
