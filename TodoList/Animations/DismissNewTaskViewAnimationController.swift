//
//  DismissNewTaskViewAnimationController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/7/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

internal class DismissNewTaskViewAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromViewController = transitionContext.viewController(forKey: .from) as? NewTaskViewController,
            let toViewController = transitionContext.viewController(forKey: .to) as? CardViewController else {
                return
        }
        let duration = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            fromViewController.setNewTaskButtonInitialPosition()
            fromViewController.newTaskContainerView.alpha = 0
            fromViewController.view.backgroundColor = .clear
        }) { (_) in
            toViewController.newTaskButtonVisible()
            fromViewController.view.removeFromSuperview()
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
}
