//
//  PresentCardViewAnimationController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/7/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

internal class PresentCardViewAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    var selectedCardFrame: CGRect = .zero
    var safeAreaWidth: CGFloat = 0.0
    var safeAreaHeight: CGFloat = 0.0
    var safeAreaTopInset: CGFloat = 0.0
    var safeAreaBottomInset: CGFloat = 0.0
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let toViewController = transitionContext.viewController(forKey: .to) as? CardViewController,
            let fromViewController = transitionContext.viewController(forKey: .from) as? POCBaseViewController else {
                return
        }
        containerView.addSubview(toViewController.view)
        toViewController.view.backgroundColor = .clear
        toViewController.cardViewContainerPositionConstraints(left: self.selectedCardFrame.origin.x,
                                                              right: self.safeAreaWidth - self.selectedCardFrame.origin.x - self.selectedCardFrame.width,
                                                              top: self.selectedCardFrame.origin.y + self.safeAreaTopInset,
                                                              bottom: toViewController.view.frame.height - toViewController.view.safeAreaInsets.top -
                                                                self.selectedCardFrame.origin.y - self.selectedCardFrame.height)
        
        toViewController.cardViewPositionConstraints(left: self.selectedCardFrame.origin.x,
                                                     right: self.safeAreaWidth - self.selectedCardFrame.origin.x - self.selectedCardFrame.width,
                                                     top: self.selectedCardFrame.origin.y,
                                                     bottom: self.safeAreaHeight -
                                                        self.selectedCardFrame.origin.y - self.selectedCardFrame.height)
        toViewController.infoStackViewMoveDown()
        toViewController.iconImageViewMoveUp()
        toViewController.backButtonNotVisible()
        toViewController.newTaskButtonNotVisible()
        
        if toViewController.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular && toViewController.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.regular {
            fromViewController.hideMainCell()
        }
        
        let duration = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration,  delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveLinear, animations: {
            fromViewController.view.alpha = 0.6
            if toViewController.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular && toViewController.traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.regular {
                fromViewController.dimNeighborCells()
                toViewController.cardViewContainerPositionConstraints(left: (fromViewController.view.frame.width * 0.35)/2,
                                                                      right: (fromViewController.view.frame.width * 0.35)/2,
                                                                      top: (fromViewController.view.frame.height * 0.25)/2,
                                                                      bottom: (fromViewController.view.frame.height * 0.25)/2)
                toViewController.cardViewPositionConstraints(left: (fromViewController.view.frame.width * 0.35)/2,
                                                             right: (fromViewController.view.frame.width * 0.35)/2,
                                                             top: (fromViewController.view.frame.height * 0.25)/2 - self.safeAreaTopInset,
                                                             bottom: (fromViewController.view.frame.height * 0.25)/2)
            } else {
                toViewController.cardViewPositionConstraints(left: 0.0,
                                                             right: 0.0,
                                                             top: 0.0,
                                                             bottom: 0.0)
                toViewController.cardViewContainerPositionConstraints(left: 0.0,
                                                                      right: 0.0,
                                                                      top: 0.0,
                                                                      bottom: 0.0)
            }
            toViewController.infoStackViewMoveUp()
            toViewController.iconImageViewMoveDown()
            toViewController.backButtonVisible()
            toViewController.newTaskButtonVisible()
        }) { (_) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
}
