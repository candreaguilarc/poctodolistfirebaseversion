//
//  SignUpScreenToHomeScreenAnimationController.swift
//  POCTodoList
//
//  Created by Carlo Aguilar on 2/22/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

internal class SignUpScreenToHomeScreenAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let fromViewController = transitionContext.viewController(forKey: .from) as? SignUpViewController,
            let toViewController = transitionContext.viewController(forKey: .to) as? POCBaseViewController else {
                return
        }
        toViewController.setAnimationConstraints()
        fromViewController.notepadImageView.isHidden = true
        containerView.addSubview(toViewController.view)
        toViewController.view.backgroundColor = .clear
        toViewController.view.bringSubviewToFront(toViewController.notepadImageView)
        toViewController.containerView.alpha = 0
        toViewController.gradientView.alpha = 0
        toViewController.notepadImageViewInitialAnimationPositionFromLoginScreen()
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            NSLayoutConstraint.deactivate(toViewController.initialPositionFromLoginSignUpScreensNotepadImageViewConstraints)
            toViewController.notepadImageViewFinalAnimationPosition()
            toViewController.containerView.alpha = 1.0
            toViewController.gradientView.alpha = 1.0
        }) { (_) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.7
    }
    
}
