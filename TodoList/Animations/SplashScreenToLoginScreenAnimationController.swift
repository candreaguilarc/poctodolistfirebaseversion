//
//  SplashScreenAnimationController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/15/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

internal class SplashScreenToLoginScreenAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let fromViewController = transitionContext.viewController(forKey: .from) as? SplashScreenViewController,
            let toViewController = transitionContext.viewController(forKey: .to) as? LoginViewController else {
                return
        }
        fromViewController.notepadImageView.alpha = 0
        toViewController.notepadImageView.alpha = 1.0
        toViewController.containerView.alpha = 0
        toViewController.gradientView.alpha = 0
        toViewController.titleLabel.alpha = 0
        toViewController.view.backgroundColor = .clear
        toViewController.notepadAnimationInitialPosition()
        containerView.addSubview(toViewController.view)
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            toViewController.titleLabel.alpha = 1.0
            toViewController.gradientView.alpha = 1.0
            toViewController.notepadImageView.alpha = 1.0
            toViewController.containerView.alpha = 1.0
            toViewController.notepadAnimationFinalPosition()
        }) { (_) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.7
    }

}
