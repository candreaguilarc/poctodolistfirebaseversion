//
//  DismissSignUpViewAnimationController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/15/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

internal class DismissSignUpAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let fromViewController = transitionContext.viewController(forKey: .from) as? SignUpViewController,
            let toViewController = transitionContext.viewController(forKey: .to) as? LoginViewController else {
                return
        }
        containerView.insertSubview(toViewController.view, at: 0)
        let duration = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, animations: {
            fromViewController.view.layoutIfNeeded()
            fromViewController.loginButton.alpha = 0
            fromViewController.containerView.alpha = 0
            fromViewController.gradientView.alpha = 0
            fromViewController.view.backgroundColor = .clear
        }) { (_) in
            fromViewController.view.removeFromSuperview()
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.8
    }
    
}
